﻿using Nop.Core;
using Nop.Core.Domain.Payments;
using Nop.Plugin.Payments.BBVAMultipagos.Models;
using Nop.Plugin.Payments.BBVAMultipagos.Validators;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Stores;
using Nop.Web.Framework.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Nop.Plugin.Payments.BBVAMultipagos.Controllers
{
    public class PaymentsBBVAMultipagosController : BasePaymentController
    {
        #region Services

        private readonly BBVAMultipagosPaymentSettings _multipagosSettings;
        private readonly ILogger _logger;
        private readonly IWorkContext _workContext;
        private readonly IStoreService _storeService;
        private readonly IOrderService _orderService;
        private readonly ISettingService _settingService;
        private readonly IPaymentService _paymentService;
        private readonly ILocalizationService _localizationService;
        private readonly IOrderProcessingService _orderProcessingService;

        private readonly PaymentSettings _paymentSettings;

        #endregion

        #region Ctor

        public PaymentsBBVAMultipagosController(
            BBVAMultipagosPaymentSettings multipagosSettings,
            IWorkContext workContext,
            IStoreService storeService,
            ISettingService settingService,
            IPaymentService paymentService,
            IOrderService orderService,
            IOrderProcessingService orderProcessingService,
            ILogger logger,
            PaymentSettings paymentSettings,
            ILocalizationService localizationService)
        {
            this._multipagosSettings = multipagosSettings;
            this._workContext = workContext;
            this._storeService = storeService;
            this._settingService = settingService;
            this._paymentService = paymentService;
            this._orderService = orderService;
            this._orderProcessingService = orderProcessingService;
            this._logger = logger;
            this._paymentSettings = paymentSettings;
            this._localizationService = localizationService;
        }

        #endregion
        
        #region Configure

        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure()
        {
            //load settings for a chosen store scope
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var MultipagosPaymentSettings = _settingService.LoadSetting<BBVAMultipagosPaymentSettings>(storeScope);

            var model = new ConfigurationModel
            {
                Cryptkey = MultipagosPaymentSettings.Cryptkey,
                AdditionalFee = MultipagosPaymentSettings.AdditionalFee,
                AdditionalFeePercentage = MultipagosPaymentSettings.AdditionalFeePercentage,
                LogDebug = MultipagosPaymentSettings.LogDebug,
                ActiveStoreScopeConfiguration = storeScope,

                pref_c_referencia = MultipagosPaymentSettings.pref_c_referencia,
                val_1 = MultipagosPaymentSettings.val_1,
                t_servicio = MultipagosPaymentSettings.t_servicio,
                c_cur = MultipagosPaymentSettings.c_cur,
                clave_entidad = MultipagosPaymentSettings.clave_entidad,
                val_16 = MultipagosPaymentSettings.val_16,
                val_19 = MultipagosPaymentSettings.val_19,
                val_20 = MultipagosPaymentSettings.val_20,
                email_admin = MultipagosPaymentSettings.email_admin,
                accion = MultipagosPaymentSettings.accion,
                puntos = MultipagosPaymentSettings.puntos
            };

            if (storeScope > 0)
            {
                model.Cryptkey_OverrideForStore = _settingService.SettingExists(MultipagosPaymentSettings, x => x.Cryptkey, storeScope);
                model.AdditionalFee_OverrideForStore = _settingService.SettingExists(MultipagosPaymentSettings, x => x.AdditionalFee, storeScope);
                model.AdditionalFeePercentage_OverrideForStore = _settingService.SettingExists(MultipagosPaymentSettings, x => x.AdditionalFeePercentage, storeScope);
                model.LogDebug_OverrideForStore = _settingService.SettingExists(MultipagosPaymentSettings, x => x.LogDebug, storeScope);

                model.pref_c_referencia_OverrideForStore = _settingService.SettingExists(MultipagosPaymentSettings, x => x.pref_c_referencia, storeScope);
                model.val_1_OverrideForStore = _settingService.SettingExists(MultipagosPaymentSettings, x => x.val_1, storeScope);
                model.t_servicio_OverrideForStore = _settingService.SettingExists(MultipagosPaymentSettings, x => x.t_servicio, storeScope);
                model.c_cur_OverrideForStore = _settingService.SettingExists(MultipagosPaymentSettings, x => x.c_cur, storeScope);
                model.clave_entidad_OverrideForStore = _settingService.SettingExists(MultipagosPaymentSettings, x => x.clave_entidad, storeScope);
                model.val_16_OverrideForStore = _settingService.SettingExists(MultipagosPaymentSettings, x => x.val_16, storeScope);
                model.val_19_OverrideForStore = _settingService.SettingExists(MultipagosPaymentSettings, x => x.val_19, storeScope);
                model.val_20_OverrideForStore = _settingService.SettingExists(MultipagosPaymentSettings, x => x.val_20, storeScope);
                model.email_admin_OverrideForStore = _settingService.SettingExists(MultipagosPaymentSettings, x => x.email_admin, storeScope);
                model.accion_OverrideForStore = _settingService.SettingExists(MultipagosPaymentSettings, x => x.accion, storeScope);
                model.puntos_OverrideForStore = _settingService.SettingExists(MultipagosPaymentSettings, x => x.puntos, storeScope);
            }

            return View("~/Plugins/Payments.BBVAMultipagos/Views/PaymentsBBVAMultipagos/Configure.cshtml", model);
        }

        [HttpPost]
        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure(ConfigurationModel model)
        {
            if (!ModelState.IsValid)
                return Configure();

            //load settings for a chosen store scope
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var MultipagosPaymentSettings = _settingService.LoadSetting<BBVAMultipagosPaymentSettings>(storeScope);

            //save settings
            MultipagosPaymentSettings.Cryptkey = model.Cryptkey;
            MultipagosPaymentSettings.AdditionalFee = model.AdditionalFee;
            MultipagosPaymentSettings.AdditionalFeePercentage = model.AdditionalFeePercentage;
            MultipagosPaymentSettings.LogDebug = model.LogDebug;

            MultipagosPaymentSettings.pref_c_referencia = model.pref_c_referencia;
            MultipagosPaymentSettings.val_1 = model.val_1;
            MultipagosPaymentSettings.t_servicio = model.t_servicio;
            MultipagosPaymentSettings.c_cur = model.c_cur;
            MultipagosPaymentSettings.clave_entidad = model.clave_entidad;
            MultipagosPaymentSettings.val_16 = model.val_16;
            MultipagosPaymentSettings.val_19 = model.val_19;
            MultipagosPaymentSettings.val_20 = model.val_20;
            MultipagosPaymentSettings.email_admin = model.email_admin;
            MultipagosPaymentSettings.accion = model.accion;
            MultipagosPaymentSettings.puntos = model.puntos;

            /* We do not clear cache after each setting update.
             * This behavior can increase performance because cached settings will not be cleared 
             * and loaded from database after each update */
            _settingService.SaveSettingOverridablePerStore(MultipagosPaymentSettings, x => x.Cryptkey, model.Cryptkey_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(MultipagosPaymentSettings, x => x.AdditionalFee, model.AdditionalFee_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(MultipagosPaymentSettings, x => x.AdditionalFeePercentage, model.AdditionalFeePercentage_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(MultipagosPaymentSettings, x => x.LogDebug, model.LogDebug_OverrideForStore, storeScope, false);

            _settingService.SaveSettingOverridablePerStore(MultipagosPaymentSettings, x => x.pref_c_referencia, model.pref_c_referencia_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(MultipagosPaymentSettings, x => x.val_1, model.val_1_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(MultipagosPaymentSettings, x => x.t_servicio, model.t_servicio_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(MultipagosPaymentSettings, x => x.c_cur, model.c_cur_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(MultipagosPaymentSettings, x => x.clave_entidad, model.clave_entidad_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(MultipagosPaymentSettings, x => x.val_16, model.val_16_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(MultipagosPaymentSettings, x => x.val_19, model.val_19_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(MultipagosPaymentSettings, x => x.val_20, model.val_20_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(MultipagosPaymentSettings, x => x.email_admin, model.email_admin_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(MultipagosPaymentSettings, x => x.accion, model.accion_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(MultipagosPaymentSettings, x => x.puntos, model.puntos_OverrideForStore, storeScope, false);

            //now clear settings cache
            _settingService.ClearCache();

            SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));

            return Configure();
        }

        #endregion

        #region paymentInfo

        [ChildActionOnly]
        public ActionResult PaymentInfo()
        {
            var model = new PaymentInfoModel();

            //years
            for (int i = 0; i < 15; i++)
            {
                string year = Convert.ToString(DateTime.Now.Year + i);
                model.ExpireYears.Add(new SelectListItem
                {
                    Text = year,
                    Value = year,
                });
            }

            //months
            for (int i = 1; i <= 12; i++)
            {
                string text = (i < 10) ? "0" + i : i.ToString();
                model.ExpireMonths.Add(new SelectListItem
                {
                    Text = text,
                    Value = i.ToString(),
                });
            }

            //set postback values
            var form = this.Request.Form;
            model.CardholderName = form["CardholderName"];
            model.CardNumber = form["CardNumber"];
            model.CardCode = form["CardCode"];
            var selectedCcType = model.CreditCardTypes.FirstOrDefault(x => x.Value.Equals(form["CreditCardType"], StringComparison.InvariantCultureIgnoreCase));
            if (selectedCcType != null)
                selectedCcType.Selected = true;
            var selectedMonth = model.ExpireMonths.FirstOrDefault(x => x.Value.Equals(form["ExpireMonth"], StringComparison.InvariantCultureIgnoreCase));
            if (selectedMonth != null)
                selectedMonth.Selected = true;
            var selectedYear = model.ExpireYears.FirstOrDefault(x => x.Value.Equals(form["ExpireYear"], StringComparison.InvariantCultureIgnoreCase));
            if (selectedYear != null)
                selectedYear.Selected = true;

            return View("~/Plugins/Payments.BBVAMultipagos/Views/PaymentsBBVAMultipagos/PaymentInfo.cshtml", model);
        }

        [NonAction]
        public override IList<string> ValidatePaymentForm(FormCollection form)
        {
            var warnings = new List<string>();

            //validate
            var validator = new PaymentInfoValidator(_localizationService);
            var model = new PaymentInfoModel
            {
                CardholderName = form["CardholderName"],
                CardNumber = form["CardNumber"],
                CardCode = form["CardCode"],
                ExpireMonth = form["ExpireMonth"],
                ExpireYear = form["ExpireYear"]
            };
            var validationResult = validator.Validate(model);
            if (!validationResult.IsValid)
                foreach (var error in validationResult.Errors)
                    warnings.Add(error.ErrorMessage);
            return warnings;
        }

        [NonAction]
        public override ProcessPaymentRequest GetPaymentInfo(FormCollection form)
        {
            var paymentInfo = new ProcessPaymentRequest();
            paymentInfo.CreditCardName = form["CardholderName"];
            paymentInfo.CreditCardNumber = form["CardNumber"];
            paymentInfo.CreditCardCvv2 = form["CardCode"];
            paymentInfo.CreditCardExpireYear = Convert.ToInt32(form["ExpireYear"]);
            paymentInfo.CreditCardExpireMonth = Convert.ToInt32(form["ExpireMonth"]);

            return paymentInfo;
        }

        #endregion
    }
}
