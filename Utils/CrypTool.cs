﻿using Nop.Core.Infrastructure;
using Nop.Services.Logging;
using System;
using System.Security.Cryptography;
using System.Text;

namespace Nop.Plugin.Payments.BBVAMultipagos.Utils
{
    public class CrypTool
    {
        public static string CrypToAES(string source)
        {
            var _multupagosSetttgins = EngineContext.Current.Resolve<BBVAMultipagosPaymentSettings>();
            var _logger = EngineContext.Current.Resolve<ILogger>();

            byte[] KeyArray = UTF8Encoding.Default.GetBytes(_multupagosSetttgins.Cryptkey.Trim());
            byte[] resultArray = new byte[0];
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(source);
            string result = "";

            try
            {
                if (KeyArray.Length > 0)
                {
                    RijndaelManaged RSAManaged = new RijndaelManaged
                    {
                        Key = KeyArray,
                        Mode = CipherMode.ECB,
                        Padding = PaddingMode.Zeros
                    };
                    ICryptoTransform cTransform = RSAManaged.CreateEncryptor();
                    resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

                    int numblock = resultArray.Length / 16;
                    byte[] datos = new byte[16];
                    
                    for (int z = 0, j = 0; z < numblock; z++)
                    {
                        for (int i = 0; i < 16; i++)
                            datos[i] = resultArray[j++];
                        result += Convert.ToBase64String(datos, 0, datos.Length);
                    }
                }
                else
                {
                    result = "OCURRIO UN ERROR DE CONFIGURACION: \n\nNO SE ENCONTRO LA LLAVE DE CIFRADO(Rijndael),\nVERIFIQUE EL ARCHIVO Rijndael.properties";
                }
            }
            catch (Exception ex)
            {
                result = "ERROR EN EL CIFRADO (AES). -> " + ex.Message;
            }

            return result;
        }

        public static string CrypToSHA1(string source)
        {
            var _multupagosSetttgins = EngineContext.Current.Resolve<BBVAMultipagosPaymentSettings>();
            var sbReturn = new StringBuilder();

            var SHA1Hasher = new HMACSHA1(new ASCIIEncoding().GetBytes(_multupagosSetttgins.Cryptkey.Trim()));
            var buffer = SHA1Hasher.ComputeHash(new ASCIIEncoding().GetBytes(source));

            var b64str = Convert.ToBase64String(buffer);
            
            for (int i = 0; i < buffer.Length; i++)
                sbReturn.Append(buffer[i].ToString("X2"));

            return b64str;
        } 
    }
}
