﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Payments.BBVAMultipagos
{
    public class BBVAMultipagosPaymentSettings : ISettings
    {
        public string Cryptkey { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to "additional fee" is specified as percentage. true - percentage, false - fixed value.
        /// </summary>
        public bool AdditionalFeePercentage { get; set; }

        /// <summary>
        /// Additional fee
        /// </summary>
        public decimal AdditionalFee { get; set; }

        public bool LogDebug { get; set; }

        /* Campos de configuracion del metodo de pago */
        
        public string pref_c_referencia { get; set; }

        public int val_1 { get; set; }

        public string t_servicio { get; set; }

        public int c_cur { get; set; }

        public int clave_entidad { get; set; }

        public string val_16 { get; set; }

        public string val_19 { get; set; }

        public string val_20 { get; set; }

        public string email_admin { get; set; }

        public string accion { get; set; }

        public int puntos { get; set; }
    }
}
