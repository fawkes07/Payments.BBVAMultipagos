﻿using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Plugins;
using Nop.Plugin.Payments.BBVAMultipagos.Controllers;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Payments;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Routing;
using static Nop.Plugin.Payments.BBVAMultipagos.Utils.CrypTool;

namespace Nop.Plugin.Payments.BBVAMultipagos
{
    public class BBVAMultipagosPaymentProcessor : BasePlugin, IPaymentMethod
    {
        #region Fields

        private readonly ILogger _logger;
        private readonly IWorkContext _workContext;
        private readonly IOrderService _orderService;
        private readonly ISettingService _settingService;
        private readonly ILocalizationService _localizationService;
        private readonly BBVAMultipagosPaymentSettings _multupagosSettings;

        #endregion

        #region Ctor

        public BBVAMultipagosPaymentProcessor(
            ILogger logger,
            IWorkContext workContext,
            IOrderService orderService,
            ISettingService settingService,
            ILocalizationService localizationService,
            BBVAMultipagosPaymentSettings multipagosSettings
            )
        {
            this._logger = logger;
            this._workContext = workContext;
            this._orderService = orderService;
            this._settingService = settingService;
            this._localizationService = localizationService;
            this._multupagosSettings = multipagosSettings;
        }

        #endregion

        #region overide methods (install & Unistall)

        public override void Install()
        {
            // Define default settings
            var settgins = new BBVAMultipagosPaymentSettings
            {
                AdditionalFee = 0,
                AdditionalFeePercentage = false,
                Cryptkey = "R?`:#*-´Ö.Úðµ©ås",
                pref_c_referencia = "eCom S0010",
                val_1 = 0,
                t_servicio = "1654",
                c_cur = 0,
                clave_entidad = 10911,
                val_16 = "1",
                val_19 = "0",
                val_20 = "0",
                email_admin = "ruben.lopez@cklass.net",
                accion = "PAGO",
                puntos = 2,
            };

            _settingService.SaveSetting(settgins);


            //locales
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.UseSandbox", "Usar modo Sandbox");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.UseSandbox.Hint", "Marque para activar modo Sandbox (Ambiente de desarrollo).");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.Cryptkey", "Llave de cifrado (RSA)");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.Cryptkey.Hint", "Esta sera la llave usada para cifrar los datos de su tarjeta");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.AdditionalFee", "Cargos adicionales");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.AdditionalFee.Hint", "Esta cantidad se adicionara a en cada compra por conceto del metodo de pago");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.AdditionalFeePercentage", "Cargos adicionales. usar porcentaje");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.AdditionalFeePercentage.Hint", "Determina si se debe aplicar un porcentaje adicional al total del pedido. Si no está activado, se utiliza un valor fijo.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.LogDebug", "Generar registro de debug");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.LogDebug.Hint", "Marcar para generar dentro del log un registro de debugeo");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.PaymentMethodDescription", "Pago con tarjeta de credito");

            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.pref_c_referencia", "pref_c_referencia");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.pref_c_referencia.Hint", "prefijo para la Referencia de pago");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.val_1", "val_1");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.val_1.Hint", "Identificador de sucursal, asignado por Flap en la configuración");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.t_servicio", "t_servicio");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.t_servicio.Hint", "Concepto de pago, asignado por Flap en la configuración");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.c_cur", "c_cur");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.c_cur.Hint", "Identificador de moneda: 0 – Pesos 1 - Dolares");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.clave_entidad", "clave_entidad");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.clave_entidad.Hint", "Identificador del comercio, asignado por FLAP en la configuración.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.val_16", "val_16");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.val_16.Hint", "Tipo de tarjeta. 1 – VISA, 2 – MASTECARD.Para otro tipo asignar un vacío.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.val_19", "val_19");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.val_19.Hint", "Indica el tipo de plan: 0 = Sin financiamiento. 1 = Financiamiento meses sin intereses.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.val_20", "val_20");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.val_20.Hint", "Número de meses en los que se dividen los pagos, para transacciones con promoción, es decir solo si val _19 es 1.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.email_admin", "email_admin");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.email_admin.Hint", "Correo del comercio.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.accion", "accion");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.accion.Hint", "Valor fijo en PAGO");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.puntos", "Config Puntos");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.punto.Hint", "0 – Compra sin puntos bin de lealtad 1 – Compra con puntos bin de lealtad, 2 – No es bin de lealtad, compra sin puntos");
            
            base.Install();
        }

        public override void Uninstall()
        {
            //settings
            _settingService.DeleteSetting<BBVAMultipagosPaymentSettings>();

            //Locales
            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.UseSandbox");
            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.UseSandbox.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.Cryptkey");
            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.Cryptkey.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.AdditionalFee");
            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.AdditionalFee.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.AdditionalFeePercentage");
            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.AdditionalFeePercentage.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.LogDebug");
            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.LogDebug.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.PaymentMethodDescription");

            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.pref_c_referencia");
            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.pref_c_referencia.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.val_1");
            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.val_1.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.t_servicio");
            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.t_servicio.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.c_cur");
            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.c_cur.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.clave_entidad");
            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.clave_entidad.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.val_16");
            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.val_16.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.val_19");
            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.val_19.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.val_20");
            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.val_20.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.email_admin");
            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.email_admin.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.accion");
            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.accion.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.puntos");
            this.DeletePluginLocaleResource("Plugins.Payments.BBVAMultipagos.Fields.puntos.Hint");

            base.Uninstall();
        }

        #endregion

        #region Propiedades y configs 

        public bool SupportCapture => false;

        public bool SupportPartiallyRefund => false;

        public bool SupportRefund => false;

        public bool SupportVoid => false;

        public RecurringPaymentType RecurringPaymentType => RecurringPaymentType.NotSupported;

        public PaymentMethodType PaymentMethodType => PaymentMethodType.Standard;

        public bool SkipPaymentInfo => false;

        public string PaymentMethodDescription => _localizationService.GetResource("Plugins.Payments.PayPalDirect.PaymentMethodDescription");

        #endregion

        #region metodos ProcessPayment

        public ProcessPaymentResult ProcessPayment(ProcessPaymentRequest processPaymentRequest)
        {
            var pedidoId = HttpContext.Current.Session["PedidoCklId"]?.ToString() ?? "";

            ProcessPaymentResult ppr = new ProcessPaymentResult();
            ppr.NewPaymentStatus = PaymentStatus.Pending;

            if (!String.IsNullOrWhiteSpace(pedidoId))
            {
                string RawResponse = "";
                var customer = _workContext.CurrentCustomer;

                try
                {
                    #region DOC estructura de objeto

                    /* *  Documentacion
                     * 
                     * s_transm                Identificador único para cada pago (manejado por el vendedor)
                     * c_referencia            Referencia de pago
                     * val_1                   Identificador de sucursal, asignado por Flap en la configuración
                     * t_servicio              Concepto de pago, asignado por Flap en la configuración (??)
                     * c_cur                   Identificador de moneda 0 -> MXN 1 -> USD
                     * t_importe               Monto de la transacción formateado a dos decimales. Ejemplo 12.20
                     * tarjetahabiente         Titular de la tarjeta, solo para tarjetas personalizadas
                     * val_3                   Número de tarjeta. Su valor debe estar cifrado Rijndael (AES).
                     * val_4                   Fecha de vencimiento en formato AAMM. Su valor debe estar cifrado Rijndael (AES).
                     * val_5                   Código de seguridad. Su valor debe estar cifrado Rijndael (AES).
                     * val_6                   160-bit HMAC SHA-1 (s_transm + c_referencia + t_importe)
                     * val_11                  Correo electrónico del cliente, es opcional
                     * val_12                  Número telefónico del cliente, es opcional
                     * clave_entidad           Identificador del comercio, asignado por FLAP en la configuración
                     * val_16                  Tipo de tarjeta. 1 – VISA, 2 – MASTECARD.Para otro tipo asignar un vacío.
                     * val_17                  Fijo en 0
                     * val_18                  SIN USO
                     * val_19                  Tipo de plan 0 Sin financiamiento / 1 MSI
                     * val_20                  Número de meses en los que se dividen los pagos, para transacciones con promoción, es decir solo si val _19 es 1
                     * email_admin             Valor fijo en PAGO
                     * accion                  FIJO en PAGO
                     * nu_afiliacion           SIN USO
                     * nu_plataforma           SIN USO 1
                     * TAG5F34                 SIN USO
                     * TAGSEMV                 SIN USO
                     * sFlag                   SIN USO
                     * TAG9F26                 SIN USO
                     * TAG9F27                 SIN USO
                     * fallback                SIN USO
                     * puntos                  0 – Compra sin puntos bin de lealtad, 1 – Compra con puntos bin de lealtad y 2 – No es bin de lealtad, compra
                     * 
                     * * * */

                    #endregion

                    using (var BBVAPaymentProcessor  = new BBVA.Multipagos.ProcesadorPagosFullClient())
                    {
                        #region DEBUG Log

                        if (_multupagosSettings.LogDebug)
                        {
                            _logger.Information("s_transm: " + processPaymentRequest.OrderGuid.ToString("N") + "\n" +
                                                "c_referencia: " + _multupagosSettings.pref_c_referencia + pedidoId + "\n" +
                                                "val_1: " + Convert.ToInt32(_multupagosSettings.val_1) + "\n" +
                                                "t_servicio: " + _multupagosSettings.t_servicio + "\n" +
                                                "c_cur: " + Convert.ToInt32(_multupagosSettings.c_cur) + "\n" +
                                                "t_importe: " + processPaymentRequest.OrderTotal.ToString("F") + "\n" +
                                                "tarjetahabiente: " + processPaymentRequest.CreditCardName + "\n" +
                                                "val_3: " + CrypToAES(processPaymentRequest.CreditCardNumber) + "\n" +
                                                "val_4: " + CrypToAES(processPaymentRequest.CreditCardExpireYear.ToString("00").Remove(0, 2) + processPaymentRequest.CreditCardExpireMonth.ToString("00")) + "\n" +
                                                "val_5: " + CrypToAES(processPaymentRequest.CreditCardCvv2) + "\n" +
                                                "val_6: " + CrypToSHA1(processPaymentRequest.OrderGuid.ToString() + "eCom S0010" + pedidoId + processPaymentRequest.OrderTotal.ToString("F") + processPaymentRequest.CreditCardCvv2) + "\n" +
                                                "val_11: " + customer.BillingAddress.Email ?? customer.Email + "\n" +
                                                "val_12: " + customer.BillingAddress.PhoneNumber ?? customer.ShippingAddress.PhoneNumber ?? "" + "\n" +
                                                "clave_entidad: " + Convert.ToInt32(_multupagosSettings.clave_entidad) + "\n" +
                                                "val_16: " + _multupagosSettings.val_16 + "\n" +
                                                "val_17: " + "0" + "\n" +
                                                "val_18: " + "" + "\n" +
                                                "val_19: " + _multupagosSettings.val_19 + "\n" +
                                                "val_20: " + _multupagosSettings.val_20 + "\n" +
                                                "email_admin: " + _multupagosSettings.email_admin + "\n" +
                                                "accion: " + _multupagosSettings.accion + "\n" +
                                                "nu_afiliacion: " + "" + "\n" +
                                                "nu_plataforma: " + "" + "\n" +
                                                "TAG5F34: " + "" + "\n" +
                                                "TAGSEMV: " + "" + "\n" +
                                                "sFlag: " + "" + "\n" +
                                                "TAG9F26: " + "" + "\n" +
                                                "TAG9F27: " + "" + "\n" +
                                                "fallback: " + 0 + "\n" +
                                                "puntos: " + Convert.ToInt32(_multupagosSettings.puntos));
                        }

                       #endregion

                        RawResponse = BBVAPaymentProcessor.procesaCompraOL(
                            s_transm: processPaymentRequest.OrderGuid.ToString("N"),
                            c_referencia: _multupagosSettings.pref_c_referencia + pedidoId,
                            val_1: Convert.ToInt32(_multupagosSettings.val_1),
                            t_servicio: _multupagosSettings.t_servicio,
                            c_cur: Convert.ToInt32(_multupagosSettings.c_cur),
                            /* INFORMACION DE VENTA */
                            t_importe: processPaymentRequest.OrderTotal.ToString("F"),
                            tarjetahabiente: processPaymentRequest.CreditCardName,
                            val_3: CrypToAES(processPaymentRequest.CreditCardNumber),
                            val_4: CrypToAES(processPaymentRequest.
                                                CreditCardExpireYear.ToString("00").Remove(0, 2) +
                                                processPaymentRequest.
                                                CreditCardExpireMonth.ToString("00")),
                            val_5: CrypToAES(processPaymentRequest.CreditCardCvv2),
                            val_6: CrypToSHA1(processPaymentRequest.OrderGuid.ToString() +
                                                "eCom S0010" + pedidoId +
                                                processPaymentRequest.OrderTotal.ToString("F") +
                                                processPaymentRequest.CreditCardCvv2),
                            val_11: customer.BillingAddress.Email ?? customer.Email,
                            val_12: customer.BillingAddress.PhoneNumber ?? customer.ShippingAddress.PhoneNumber ?? "",
                            /* FIN INFORMACION DE VENTA */
                            clave_entidad: Convert.ToInt32(_multupagosSettings.clave_entidad),
                            val_16: _multupagosSettings.val_16,
                            val_17: "0",
                            val_18: "",
                            val_19: _multupagosSettings.val_19,
                            val_20: _multupagosSettings.val_20,
                            email_admin: _multupagosSettings.email_admin,
                            accion: _multupagosSettings.accion,
                            nu_afiliacion: "",
                            nu_plataforma: "",
                            TAG5F34: "",
                            TAGSEMV: "",
                            sFlag: "",
                            TAG9F26: "",
                            TAG9F27: "",
                            fallback: 0,
                            puntos: Convert.ToInt32(_multupagosSettings.puntos)
                        );
                    }
                }
                catch (Exception ex)
                {
                    ppr.Errors.Add("Error 0x2210000: Imposible realizar su pago, intente mas tarde!");
                    _logger.Warning(ex.Message, ex, _workContext.CurrentCustomer);
                }

                if (!String.IsNullOrWhiteSpace(RawResponse))
                {
                    /*  Valores de respuesta
                     *  
                     *  [0] Número de autorización   -   AN F 6      -   Si la venta es declinada o no es procesada su valor será 000000, en caso contrario será distinta de 6 ceros.
                     *  [1] Mensaje                  -   A V 0:100   -   Para transacciones declinadas o no procesadas, contiene  el motivo del rechazo, para transacciones aprobada contiene el mensaje APROBADA
                     *  [2] Código de respuesta      -   N F 2       -   Código de respuesta, si la operación fue aprobada el código será 00, un código distinto es declinado.
                     *  [3] Imprimir                 -   A F 2       -   SI – Imprimir pagare NO – No imprimir pagare
                     *  [4] Fecha                    -               -   Fecha en formato DDMMMAA
                     *  [5] Hora                     -               -   Hora en formato HH:MM
                     */
                    
                    var response = RawResponse.Split('|');

                    // PROCESAMIENTO DE LA VANTA
                    var estatus = response[1];

                    if (estatus.Equals("APROBADA"))
                    {
                        // CARGO EXITOSO
                        ppr.NewPaymentStatus = PaymentStatus.Paid;
                        ppr.AllowStoringCreditCardNumber = true;
                        ppr.AuthorizationTransactionCode = response[0];
                        ppr.AuthorizationTransactionId = response[0];
                        ppr.AuthorizationTransactionResult = RawResponse;
                    }
                    else
                    {
                        // CARGO DECLINADO
                        ppr.Errors.Add(response[1]);
                        if (_multupagosSettings.LogDebug)
                            _logger.Warning(RawResponse);
                    }
                }
                else
                {
                    ppr.Errors.Add("Error 0x2903: Imposible realizar su pago, intente mas tarde!");
                    
                    _logger.Warning("Multipagos no esta respondiendo!!", null, _workContext.CurrentCustomer);
                }
            }
            return ppr;
        }

        public void PostProcessPayment(PostProcessPaymentRequest postProcessPaymentRequest)
        {
            postProcessPaymentRequest.Order.CardCvv2 = "***";
            postProcessPaymentRequest.Order.CardExpirationMonth = "**";
            postProcessPaymentRequest.Order.CardExpirationYear = "****";

            _orderService.UpdateOrder(postProcessPaymentRequest.Order);
        }

        public bool HidePaymentMethod(IList<ShoppingCartItem> cart)
        {
            return false;
        }

        public bool CanRePostProcessPayment(Order order)
        {
            return false;
        }

        public decimal GetAdditionalHandlingFee(IList<ShoppingCartItem> cart)
        {
            return decimal.Zero;
        }

        public Type GetControllerType()
        {
            return typeof(PaymentsBBVAMultipagosController);
        }

        #endregion

        #region Rutas

        public void GetConfigurationRoute(out string actionName, 
                                          out string controllerName, 
                                          out RouteValueDictionary routeValues)
        {
            actionName = "Configure";
            controllerName = "PaymentsBBVAMultipagos";
            routeValues = new RouteValueDictionary
                {
                    { "Namespaces", "Nop.Plugin.Payments.BBVAMultipagos.Controllers" },
                    { "area", null }
                };
        }

        public void GetPaymentInfoRoute(out string actionName, 
                                        out string controllerName, 
                                        out RouteValueDictionary routeValues)
        {
            actionName = "PaymentInfo";
            controllerName = "PaymentsBBVAMultipagos";
            routeValues = new RouteValueDictionary
                {
                    { "Namespaces", "Nop.Plugin.Payments.BBVAMultipagos.Controllers" },
                    { "area", null }
                };
        }

        #endregion

        #region Metodos no soportados

        public CancelRecurringPaymentResult CancelRecurringPayment(CancelRecurringPaymentRequest cancelPaymentRequest)
        {
            throw new NotImplementedException();
        }

        public CapturePaymentResult Capture(CapturePaymentRequest capturePaymentRequest)
        {
            throw new NotImplementedException();
        }
        
        public ProcessPaymentResult ProcessRecurringPayment(ProcessPaymentRequest processPaymentRequest)
        {
            throw new NotImplementedException();
        }

        public RefundPaymentResult Refund(RefundPaymentRequest refundPaymentRequest)
        {
            throw new NotImplementedException();
        }

        public VoidPaymentResult Void(VoidPaymentRequest voidPaymentRequest)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}