﻿using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Payments.BBVAMultipagos.Models
{
    public class ConfigurationModel : BaseNopModel
    {
        public int ActiveStoreScopeConfiguration { get; set; }
        
        [NopResourceDisplayName("Plugins.Payments.BBVAMultipagos.Fields.UseSandbox")]
        public bool UseSandbox { get; set; }
        public bool UseSandbox_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BBVAMultipagos.Fields.Cryptkey")]
        public string Cryptkey { get; set; }
        public bool Cryptkey_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BBVAMultipagos.Fields.AdditionalFeePercentage")]
        public bool AdditionalFeePercentage { get; set; }
        public bool AdditionalFeePercentage_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BBVAMultipagos.Fields.AdditionalFee")]
        public decimal AdditionalFee { get; set; }
        public bool AdditionalFee_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BBVAMultipagos.Fields.LogDebug")]
        public bool LogDebug { get; set; }
        public bool LogDebug_OverrideForStore { get; set; }

        /* Opciones de configuracion del metodo de pago */

        [NopResourceDisplayName("Plugins.Payments.BBVAMultipagos.Fields.pref_c_referencia")]
        public string pref_c_referencia { get; set; }
        public bool pref_c_referencia_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BBVAMultipagos.Fields.val_1")]
        public int val_1 { get; set; }
        public bool val_1_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BBVAMultipagos.Fields.t_servicio")]
        public string t_servicio { get; set; }
        public bool t_servicio_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BBVAMultipagos.Fields.c_cur")]
        public int c_cur { get; set; }
        public bool c_cur_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BBVAMultipagos.Fields.clave_entidad")]
        public int clave_entidad { get; set; }
        public bool clave_entidad_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BBVAMultipagos.Fields.val_16")]
        public string val_16 { get; set; }
        public bool val_16_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BBVAMultipagos.Fields.val_19")]
        public string val_19 { get; set; }
        public bool val_19_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BBVAMultipagos.Fields.val_20")]
        public string val_20 { get; set; }
        public bool val_20_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BBVAMultipagos.Fields.email_admin")]
        public string email_admin { get; set; }
        public bool email_admin_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BBVAMultipagos.Fields.accion")]
        public string accion { get; set; }
        public bool accion_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BBVAMultipagos.Fields.puntos")]
        public int puntos { get; set; }
        public bool puntos_OverrideForStore { get; set; }
    }
}
